package fptedu.swp391_gr2_se1736.swp391pj.model;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Optional;
import java.util.Set;

import jakarta.persistence.*;
import org.apache.commons.codec.binary.Base64;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
//@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer courseid;

    private String coursename;

    @ManyToOne
    @JoinColumn(name = "categoryid")
    private Category category;

    private Date createdate;

    private Double courseprice;

    @ManyToOne
    @JoinColumn(name = "courseownerid")
    private Courseowner courseowner;

    private String description;

    private byte[] image;

    private Integer courserating;

    private Integer coursestatus;

    @OneToMany(mappedBy="course")
    private Set<Discount> discountSet;

    public String showImage(){
        if(image != null){
            byte[] encodebase64 = Base64.encodeBase64(image);
            String base64encoded = new String(encodebase64, StandardCharsets.UTF_8);
            return base64encoded;
        }
        return null;
    }

    public Double getCurrentPrice(){
        Optional<Discount> optionalDiscount = discountSet.stream().filter(discount -> discount.getStartdate().compareTo(new Date()) <= 0 && discount.getEnddate().compareTo(new Date()) >= 0).findFirst();
        if(optionalDiscount.isPresent()){
            return courseprice * (100 - optionalDiscount.get().getDiscountvalue());
        }
        return null;
    }

    // Thêm getters và setters
}
