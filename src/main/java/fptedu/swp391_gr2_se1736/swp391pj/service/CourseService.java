package fptedu.swp391_gr2_se1736.swp391pj.service;

import fptedu.swp391_gr2_se1736.swp391pj.model.Course;
import fptedu.swp391_gr2_se1736.swp391pj.respository.CourseRespository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class CourseService {
    private final CourseRespository courseRespository;

    public CourseService(CourseRespository courseRespository) {
        this.courseRespository = courseRespository;
    }

    public Page<Course> findPaginated(Pageable pageable,List<Course> courses) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Course> list;

        if (courses.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, courses.size());
            list = courses.subList(startItem, toIndex);
        }

        Page<Course> coursePage
                = new PageImpl<Course>(list, PageRequest.of(currentPage, pageSize), courses.size());
        return coursePage;
    }

    public List<Course> getAll(){
        return courseRespository.findAll();
    }

    public List<Course> getCourseByCategory(Integer categoryID){
        return courseRespository.findCourseByCategory(categoryID);
    }

    public List<Course> getCourseBySearchValue(String searchValue){
        return courseRespository.findCourseBySearchValue(searchValue);
    }

    public Course getCourseByID(int id){
        return courseRespository.findById(id).get();
    }
}
