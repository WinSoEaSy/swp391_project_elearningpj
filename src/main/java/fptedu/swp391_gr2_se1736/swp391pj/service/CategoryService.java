package fptedu.swp391_gr2_se1736.swp391pj.service;

import fptedu.swp391_gr2_se1736.swp391pj.model.Category;
import fptedu.swp391_gr2_se1736.swp391pj.respository.CategoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public List<Category> getAll(){
        return categoryRepository.findAll();
    }
}
