package fptedu.swp391_gr2_se1736.swp391pj.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fptedu.swp391_gr2_se1736.swp391pj.model.Course;

import java.util.List;

@Repository
public interface CourseRespository extends JpaRepository<Course, Integer>{
    @Query(value = "SELECT * FROM course where categoryid = :categoryID",nativeQuery = true)
    public List<Course> findCourseByCategory(Integer categoryID);

    @Query(value = "SELECT * FROM course where coursename like %:value%",nativeQuery = true)
    public List<Course> findCourseBySearchValue(String value);
}
