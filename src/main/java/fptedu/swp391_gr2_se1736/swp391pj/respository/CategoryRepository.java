package fptedu.swp391_gr2_se1736.swp391pj.respository;

import fptedu.swp391_gr2_se1736.swp391pj.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
}
