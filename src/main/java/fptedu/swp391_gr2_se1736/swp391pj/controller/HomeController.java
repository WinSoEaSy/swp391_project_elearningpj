package fptedu.swp391_gr2_se1736.swp391pj.controller;

import fptedu.swp391_gr2_se1736.swp391pj.model.Category;
import fptedu.swp391_gr2_se1736.swp391pj.model.Course;
import fptedu.swp391_gr2_se1736.swp391pj.respository.CategoryRepository;
import fptedu.swp391_gr2_se1736.swp391pj.respository.CourseRespository;
import fptedu.swp391_gr2_se1736.swp391pj.service.CategoryService;
import fptedu.swp391_gr2_se1736.swp391pj.service.CourseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping(path = "")
public class HomeController {
    private final CategoryService categoryService;

    private final CourseService courseService;

    public HomeController(CategoryService categoryService, CourseService courseService) {
        this.categoryService = categoryService;
        this.courseService = courseService;
    }

    @GetMapping("/")
    public ModelAndView showSignupPage(){
        Map<String, Object> modelMap = new HashMap<>();
        List<Category> listCategory = categoryService.getAll();
        List<Course> listCourses = courseService.getAll();
        modelMap.put("listCate",listCategory);
        modelMap.put("listCourse",listCourses);
        ModelAndView mv = new ModelAndView("client/index",modelMap);
        return mv;
    }

    @GetMapping("/courses")
    public ModelAndView showAllCourse(@RequestParam(name = "categoryID",required = false) Integer categoryID,
                                      @RequestParam(name = "searchValue",required = false) String searchValue,
                                      @RequestParam(value = "page",required = false) Integer page)
    {
        Map<String, Object> modelMap = new HashMap<>();
        List<Course> courses = new ArrayList<>();
        if(categoryID != null){
            modelMap.put("categoryID",categoryID);
            courses = courseService.getCourseByCategory(categoryID);
        }
        if(searchValue != null){
            modelMap.put("searchValue",searchValue);
            courses = searchValue.trim().isEmpty() ? courseService.getAll() : courseService.getCourseBySearchValue(searchValue);
        }
        Page<Course> paginated = courseService.findPaginated(PageRequest.of(page != null ? page - 1 : 0, 9), courses);
        List<Category> listCategory = categoryService.getAll();
        int totalPages = paginated.getTotalPages();
        int pageNum = page != null ? page : 1;
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(pageNum, Math.min(totalPages,pageNum + 2))
                    .boxed()
                    .collect(Collectors.toList());
            modelMap.put("pageNumbers", pageNumbers);
        }
        modelMap.put("listCate",listCategory);
        modelMap.put("pagingCourses",paginated);
        ModelAndView mv = new ModelAndView("client/courses",modelMap);
        return mv;
    }

    @GetMapping("/courseDetail/{id}")
    public ModelAndView getCourseByID(@PathVariable(value = "id") int id) {
        Map<String, Object> modelMap = new HashMap<>();
        Course course = courseService.getCourseByID(id);
        List<Category> listCategory = categoryService.getAll();
        modelMap.put("course",course);
        modelMap.put("listCate",listCategory);
        ModelAndView mv = new ModelAndView("client/courses-details",modelMap);
        return mv;
    }
}
